require "./keyboard.cr"
require "colorize"

module KeysmashAnalyzer
  VERSION = "0.1.0"
  # uses ANSI escape codes to suggest default layout
  print "Please enter path to a layout: \x1b[s#{"layouts/en_us.txt".colorize(:dark_gray)}\x1b[u"
  input = gets
  # fall back to en_us.txt if no input is provided
  input = "layouts/en_us.txt" if input.nil? || input.blank?
  keyboard = Keyboard.new input
  print "Please keysmash into your terminal: "
  keysmash = gets
  exit if keysmash.nil?
  keysmash = keysmash.downcase
  # set usage frequency for each char
  keysmash.chars.each do |c|
    keyboard.layout[c] += 1
  end
  # print the heatmap
  puts keyboard
end
