class Keyboard
  property layout
  property raw_layout : String
  # path to layout file
  setter name : String

  def initialize(@name)
    # character and frequency
    @layout = {} of Char => Int32
    @raw_layout = File.read(name)
    i = 0
    c = @raw_layout.chars

    # iterates over characters in layout
    while i < c.size
      # outputs a hashmap of characters inside [] in the layout and sets each to 0 uses
      if c[i] == '['
        until c[i + 1] == ']'
          # character escape
          if c[i + 1] == '\\'
            i += 1
          end
          @layout[c[i += 1]] = 0
        end
      end
      i += 1
    end
  end

  def to_s(io : IO)
    # remove character escapes
    formatted = @raw_layout.gsub(/\\(\S)/, &.[1])

    # an rgb gradient from purple to red
    # defines the heatmap colors
    colors = [
      {74, 16, 99},
      {113, 30, 114},
      {137, 40, 107},
      {160, 50, 100},
      {183, 60, 94},
      {207, 70, 87},
      {230, 80, 80},
    ]

    # add a keypress heatmap
    max = @layout.to_a.sort_by { |_, v| v }.reverse.to_h.values[0]
    formatted = formatted.gsub(/\[(.{1,2})\]/) { |m|
      # calculate proportion of key usage to colors
      char_color = colors[((@layout[m.chars[1]] * (colors.size - 1)) / max).to_i]

      # set the key color
      m.colorize.fore(:white).back(Colorize::ColorRGB.new(char_color[0].to_u8, char_color[1].to_u8, char_color[2].to_u8))
    }

    # output the result
    io << formatted
  end
end
