# KeysmashAnalyzer

A CLI tool that generates a heatmap from a keysmash.

<img src="https://i.imgur.com/xQX5zZh.png" height="200">

## Installation

```sh
git clone https://gitlab.com/agathasorceress/keysmash-analyzer
cd keysmash-analyzer/
crystal build src/keysmash-analyzer.cr
```

## Usage

```sh
./keysmash-analyzer
```
You can define custom layouts, use ``layouts/en_us.txt`` as reference.
